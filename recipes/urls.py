from django.urls import path
from .views import show_recipe, recipe_list, create_recipe, edit_recipe, my_recipe_list

urlpatterns = [
    path("recipes/", recipe_list, name="recipes_list"),
    path("recipes/<int:id>/", show_recipe, name="show_recipe"),
    path("recipes/create/", create_recipe, name="create"),
    path("recipes/<int:id>/edit/", edit_recipe, name="edit"),
    path("recipes/mine/", my_recipe_list, name="my_recipe_list")
]
